<?php

namespace Server;

class SimpleServer
{
    const SHUTDOWN_COMMAND = 'EXIT';
    const UPTIME_COMMAND = 'UPTIME';
    const BIND_ADDRESS = '0.0.0.0';

    private $port;
    private $requestHandler;

    private $shouldShutDown = false;
    private $startTimestamp;

    public function __construct($port, SimpleRequestHandler $requestHandler)
    {
        $this->port = $port;
        $this->requestHandler = $requestHandler;
        $this->startTimestamp = microtime(true);
    }

    public function run()
    {
        /* Allow the script to hang around waiting for connections. */
        set_time_limit(0);

        /* Turn on implicit output flushing so we see what we're getting
         * as it comes in. */
        ob_implicit_flush();

        try {
            $this->runInternal();
        } catch (Exception $e) {
            throw new RuntimeException($e);
        }
    }

    private function runInternal()
    {
        try {
            if (($listener = stream_socket_server('tcp://'.self::BIND_ADDRESS.':'.$this->port, $errno, $errstr)) === false) {
                throw new Exception("socket_create() failed: reason: $errstr ($errno)");
            }

            while (!$this->shouldShutDown) {
                $connection = @stream_socket_accept($listener);

                if ($connection) {
                    $output = $this->handleRequest($connection);

                    fclose($connection);
                }
            }
        } finally {
            if ($listener !== false) {
                fclose($listener);
            }
        }
    }

    private function handleRequest($connection)
    {
        $buf = fgets($connection);

        $buf = trim($buf);

        if (ctype_space($buf)) {
            return;
        }

        if ($buf == self::SHUTDOWN_COMMAND) {
            $this->shouldShutDown = true;

            return;
        }

        if ($buf == self::UPTIME_COMMAND) {
            $response = (int)floor(microtime(true) - $this->startTimestamp);
        } else {
            $response = $this->requestHandler->processRequest($buf);
        }

        fwrite($connection, $response."\n");
    }
}
