<?php

namespace Ai;
use Game\Point;

class MBBotAi implements BotAi
{
	public $myId = null;
	public $gamePlan = null;
	public $players = null;  // array of positions representing players
	public $myPosition = null;
	public $round = 0;
	public $enemyFired = false;

	public $up = 'U';
	public $down = 'D';
	public $left = 'L';
	public $right = 'R';
	public $hold = 'X';

	public $gameBoard = array();
	public $movePattern = array('Up', 'Left', 'Left');
	public $movePatternPosition = 0;
	public $movePatterns = array(
		0 => array('D', 'D', 'L', 'L', 'L', 'L', 'U','U', 'U', 'U', 'U', 'R', 'R', 'R', 'R', 'D', 'D'),
		1 => array(
			'R', 'R', 'R',
			'D', 'D',
			'L', 'L',
			'D',
			'R', 'R',
			'D', 'D',
			'L', 'L',
			'U',
			'L',
			'D', 'D', 'D',
			'R', 'R', 'R',
			'U',
			'R',
			'D',
			'R', 'R',
			'U', 'U',
			'L',
			'U',
			'R', 'R',
			'U', 'U', 
			'L', 'L', 
			'U', 'U', 
			'R', 'R', 
			'D'
			)
		);
	public $moveState = 0;
	public $positions = array();

	public $boardState = array();
	public $lastMove = "U";
	/**
	 * This method is called at the beginning of the game.
	 *
	 * @param GameSetup $gameSetup details of the game, such as the game plan dimensions, the list of players, etc
	 *
	 * @return the first move to play
	 */
	public function initializeAndMakeMove($gameSetup)
	{
		$this->myId = $gameSetup->aiPlayerId;
		$this->gamePlan = $gameSetup->gamePlan;
		$this->players = $gameSetup->players;
		$this->boardState = $this->emptyBoard($gameSetup->gamePlan->width, $gameSetup->gamePlan->height);

		foreach ($this->players as $i => $player) {
			$this->positions[$player->id] = $gameSetup->gamePlan->startingPositions[$i];
		}


		$this->round = 1;
		$this->movePattern = array_merge($this->movePatterns[0], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1], $this->movePatterns[1]);

		// $this->setMyStartingPosition();

		$this->updateBoardState();

		return $this->figureOutMove();
	}

	/**
	 * This method is called in each round, except for the first one.
	 *
	 * @param $gameRound all players' moves in the previous round
	 *
	 * @return the move to play
	 */
	public function makeMove($gameRound)
	{
		$this->updatePositions($gameRound->moves);
		$this->updateBoardState();
		// return our move for the next round
		$this->lastMove = $this->figureOutMove();
		return $this->lastMove;

	}

	public function updatePositions($round) {
		foreach ($round as $move) {
			$b = $this->updatePosition($this->positions[$move->playerId], $move->move);
			$this->positions[$move->playerId] = $b;
			// if ($move->move == 'U') {
			// 	$this->positions[$move->playerId]->y--;
			// } elseif ($move->move == 'D') {
			// 	$this->positions[$move->playerId]->y++;
			// } elseif ($move->move == 'L') {
			// 	$this->positions[$move->playerId]->x--;
			// } elseif ($move->move == 'R') {
			// 	$this->positions[$move->playerId]->x++;
			// }
			// $this->positions[$move->playerId] = $this->wrapCoordinates($this->positions[$move->playerId]->x);
		}
	}
	public function updatePosition($position, $move) {
		$position = clone $position;
		if ($move == 'U') {
			$position->y--;
		} elseif ($move == 'D') {
			$position->y++;
		} elseif ($move == 'L') {
			$position->x--;
		} elseif ($move == 'R') {
			$position->x++;
		}
		$position = $this->wrapCoordinates($position);
		return $position;
	}

	public function wrapCoordinates($point) {
		if ($point->x < 0) {
			$point->x = $this->gamePlan->width - 1;
		} elseif ($point->x == $this->gamePlan->width) {
			$point->x = 0;
		}
		if ($point->y < 0) {
			$point->y = $this->gamePlan->height - 1;
		} elseif ($point->y == $this->gamePlan->height) {
			$point->y = 0;
		}
		return $point;
	}

	public function setMyStartingPosition()
	{
		$player = new \Game\Player($this->myId, null);
		$myIndex = array_search($player, $this->players);
		$this->myPosition = $this->gamePlan->startingPositions[$myIndex];
	}

	public function figureOutMove()
	{
		$move = array_shift($this->movePattern);
		if ($this->moveIsGood($move)) {
			return $move;
		}
		return $this->moveToFirstEmptySpace();
		// $this->moveToFirstEmptySpace();
	}

	public function moveIsGood($move) {
		$newPoint = $this->updatePosition($this->positions[$this->myId], $move);
		if ($this->boardState[$newPoint->x][$newPoint->y] === false) {
			return true;
		}
		return false;
	}

	public function moveToFirstEmptySpace() {
		$possibleMoves = array('U', 'D', 'L', 'R');
		shuffle($possibleMoves);
		foreach ($possibleMoves as  $direction) {
			if ($this->moveIsGood($direction)) {
				return $direction;
			}
		}
		return $this->doWhatever();
	}

	public function doWhatever() {
		if (rand(0,1)) {
			return 'R';
		}
		return 'U';
	}

	public function emptyBoard($width, $height) {
		$board = array();
		for ($x = 0; $x < $width; $x++) {
			for($y = 0; $y < $height; $y++) {
				$board[$x][$y] = false;
			}
		}

		return $board;
	}

	public function updateBoardState() {
		foreach($this->positions as $playerId => $point) {
			if ($this->boardState[$point->x][$point->y] === false) {
				$this->boardState[$point->x][$point->y] = $playerId;
			}
		}
	}

	public function openNeighbours($x, $y) {
		$point = new Point($x, $y);
		$score = 0;
		$directions = array(
		 array('U'),
		 array('U', 'R'),
		 array('R'),
		 array('R', 'D'),
		 array('D'),
		 array('D', 'L'),
		 array('L'),
		 array('L','U')
		);
		foreach ($directions as $dir) {
			foreach($dir as $move) {
				$this->updatePosition($point, $move);
			}
			if (empty($this->boardState[$newPoint->x][$newPoint->y])) {
				$score++;
			}
		}

		return $score;
	}

}
