<?php

namespace Game;

class GameRound
{
    // a collection of PlayerMoves
    public $moves = array();

    /**
     * GameRound constructor.
     *
     * @param $moves array containing PlayerMove objects
     *
     * @return GameRound
     */
    public function __construct($moves)
    {
        $this->moves = $moves;
    }

    /**
     * constructs a GameRound using a data array
     *
     * @param $props array containing player moves
     * e.g.
     * {
     * 		"moves":[
     * 			{"playerId":1,"move":"FIRE"},
     *			{"playerId":2,"move":"JUMP"}
     *		]
     *	}
     *
     * @return GameRound
     */
    public static function fromHash($props)
    {
        $moves = [];
        foreach ($props['moves'] as $move) {
            $moves[] = PlayerMove::fromHash($move);
        }

        return new GameRound($moves);
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'moves' => $this->moves,
            )
        );

        return 'GameRound'.$json;
    }
}
