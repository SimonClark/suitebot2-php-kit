<?php

namespace Game;

class Player
{
    public $id = null;
    public $name = '';

    /**
     * Player constructor.
     *
     * @param  integer  id   identifier of the player
     * @param  string   name name of the player
     *
     * @return Player
     **/
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @param $o
     *
     * @return bool true if passed itself
     */
    public function equals($o)
    {
        if ($this === $o) {
            return true;
        }
        if ($o == null) {
            return false;
        }
        if (get_class($o) != 'Player') {
            return false;
        }

        return $o->id == $this->id;
    }

    /**
     * constructs a Player using a data array
     *
     * @param array $props an array containing id and name
     *  e.g. (expressed as JSON):
     *  {
     *      "id": 1,
     *      "name": "Griffindor",
     *  }
     *
     * @return Player
     */
    public static function fromHash($hash)
    {
        return new Player($hash['id'], $hash['name']);
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'id' => $this->id,
                'name' => $this->name,
            )
        );

        return 'Player'.$json;
    }
}
