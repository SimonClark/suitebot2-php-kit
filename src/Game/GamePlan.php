<?php

namespace Game;

class GamePlan
{
    public $width = 0;
    public $height = 0;
    public $maxRounds = 0;
    public $startingPositions = array();
    /**
     * GamePlan constructor.
     *
     * @param integer $width              width of the game board
     * @param integer $height             height of the game board
     * @param array   $startingPositions  array of Points
     * @param integer $maxRounds          how many rounds this game will have
     *
     * @return GamePlan
     */
    public function __construct($width, $height, $startingPositions, $maxRounds)
    {
        $this->width = $width;
        $this->height = $height;
        $this->startingPositions = $startingPositions;
        $this->maxRounds = $maxRounds;
    }

    /**
     * constructs a GamePlan using a data array
     *
     * @param array $props an array containing width, height, startingPositions and maxRounds
     *  e.g. (expressed as JSON):
     *  {
     *      "width":10,
     *      "height":20,
     *      "startingPositions":[
     *          {"x":2,"y":2},
     *          {"x":7,"y":7}
     *      ],
     *      "maxRounds":25
     *  }
     *
     * @return GamePlan
     */
    public static function fromHash($props)
    {
        $startingPositions = [];
        if (isset($props['startingPositions']))
        {
            foreach ($props['startingPositions'] as $point)
            {
                $startingPositions[] = Point::fromHash($point);
            }
        }
        return new GamePlan($props['width'], $props['height'], $startingPositions, $props['maxRounds']);
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'width' => $this->width,
                'height' => $this->height,
                'startingPositions' => $this->startingPositions,
                'maxRounds' => $this->maxRounds,
            )
        );

        return 'GamePlan'.$json;
    }
}
