<?php

namespace Game;

class Point
{
    public $x = null;
    public $y = null;

    /**
     * Point constructor.
     *
     * @param $x
     * @param $y
     *
     * @return Point
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * constructs a Point using a data array
     *
     * @param array $props an array containing x and y
     * e.g. (expressed as JSON):
     * {
     *     "x":2,
     *     "y":2
     * }
     *
     * @return Point
     */
    public static function fromHash($props)
    {
        return new Point($props['x'], $props['y']);
    }

    /**
     * Two points are considered equal if they occupy the same x and y coordinates.
     *
     * @param Point $point
     *
     * @return bool
     */
    public function equals($point)
    {
        if ($this === $point) {
            return true;
        }
        if ($point == null) {
            return false;
        }
        if (!($point instanceof Point)) {
            return false;
        }

        return $point->x == $this->x && $point->y == $this->y;
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'x' => $this->x,
                'y' => $this->y,
            )
        );

        return 'Point'.$json;
    }
}
