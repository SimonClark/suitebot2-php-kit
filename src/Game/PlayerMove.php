<?php

namespace Game;

class PlayerMove
{
    public $playerId = null;
    public $move = '';

    /**
     * PlayerMove constructor.
     *
     * @param integer  $playerId  identifier of the player who made this move
     * @param string   $move      a command
     *
     * @return PlayerMove
     */
    public function __construct($playerId, $move)
    {
        $this->playerId = $playerId;
        $this->move = $move;
    }

    /**
     * constructs a GamePlan using a data array
     *
     * @param array $props an array containing width, height, startingPositions and maxRounds
     *  e.g. (expressed as JSON):
     *  {
     *      "playerId":1,
     *      "move": "FIRE"
     *  }
     *
     * @return PlayerMove
     */
    public static function fromHash($props)
    {
        return new PlayerMove($props['playerId'], $props['move']);
    }
    public function __toString()
    {
        $json = json_encode(
            array(
                'playerId' => $this->playerId,
                'move' => $this->move,
            )
        );

        return 'PlayerMove'.$json;
    }
}
