<?php

namespace Game;

class GameSetup
{
    public $aiPlayerId = null;
    public $players = array();
    public $gamePlan = null;

    /**
     * GameSetup constructor.
     *
     * @param integer $playerId   the id of self
     * @param array   $players    array of Player objects
     * @param GamePlan $gamePlan  the game plan
     *
     * @return GameSetup
     */
    public function __construct($playerId, $players, $gamePlan)
    {
        $this->aiPlayerId = $playerId;
        $this->players = $players;
        $this->gamePlan = $gamePlan;
    }

    /**
     * Constructs a GameSetup using a data array
     *
     * @param array $props an array, containing aiPlayerId, players, and gamePlan
     * e.g.
     * {
     * 		"aiPlayerId":1,
     *		"players":[
     *			{
     *				"id":2,
     *				"name":"R2D2"
     *			},
     *			{
     *				"id":1,
     *				"name":"C-3PO"
     *			}
     *		],
     *		"gamePlan":{
     *			"width":10,
     *			"height":20,
     *			"startingPositions":[
     *				{"x":2,"y":2},
     *				{"x":7,"y":7}
     *			],
     *			"maxRounds":25
     *		}
     * }
     *
     * @return GameSetup
     */
    public static function fromHash($props)
    {
        $players = [];
        foreach ($props['players'] as $player) {
            $players[] = Player::fromHash($player);
        }
        return new GameSetup($props['aiPlayerId'], $players, GamePlan::fromHash($props['gamePlan']));
    }

    public function __toString()
    {
        $json = json_encode(
            array(
                'aiPlayerId' => $this->aiPlayerId,
                'players' => $this->players,
                'gamePlan' => $this->gamePlan,
            )
        );

        return 'GameSetup'.$json;
    }
}
