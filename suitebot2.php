<?php

require __DIR__.'/vendor/autoload.php';

define('DEFAULT_PORT', 9001);

function determinePort($argv)
{
    if (count($argv) == 2) {
        return intval($argv[1]);
    } else {
        return DEFAULT_PORT;
    }
}

$botAi = new Ai\MBBotAi();

$port = determinePort($argv);

echo "listening on port $port";

(new Server\SimpleServer($port, new BotRequestHandler($botAi)))->run();
