<?php

use PHPUnit\Framework\TestCase;
use Game\Player;
use Game\PlayerMove;
use Game\Point;
use Game\GamePlan;
use Game\GameSetup;
use Game\GameRound;
use Ai\MBBotAi;

class MBBotAiTest extends TestCase
{
	const MY_ID = 1;
	const PLAN_WIDTH = 20;
	const PLAN_HEIGHT = 20;
	const MAX_ROUNDS = 4;

    private static $PLAYERS, $STARTING_POSITIONS, $GAME_PLAN, $GAME_SETUP, $GAME_ROUND;

	private $botAi;

    public static function setUpBeforeClass()
    {
        self::$PLAYERS = [new Player(1, "Me"), new Player(2, "Enemy 1"), new Player(3, "Enemy 2")];
        self::$STARTING_POSITIONS = [new Point(1, 1), new Point(2, 2), new Point(3, 3)];
        self::$GAME_PLAN = new GamePlan(self::PLAN_WIDTH, self::PLAN_HEIGHT, self::$STARTING_POSITIONS, self::MAX_ROUNDS);
        self::$GAME_SETUP = new GameSetup(self::MY_ID, self::$PLAYERS, self::$GAME_PLAN);
        self::$GAME_ROUND = new GameRound(array(new PlayerMove(1,'D'),new PlayerMove(2,'D'),new PlayerMove(3,'L')));


    }

    public function setUp()
    {
        $this->botAi = new MBBotAi();
    }

	public function testInitializeAndMakeMove()
	{
		$move = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$this->AssertEquals($move, 'D');
		$move = $this->botAi->makeMove(self::$GAME_ROUND);
		$this->AssertEquals($move, 'D');
		$move = $this->botAi->makeMove(self::$GAME_ROUND);
		$this->AssertEquals($move, 'L');
		$move = $this->botAi->makeMove(self::$GAME_ROUND);
		$this->AssertEquals($move, 'L');
	}

	public function testUpdatePosition() {
		$move = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$move1 = $this->botAi->updatePosition(new Point(0, 0), 'U');
		$this->AssertEquals($move1->x, 0);
		$this->AssertEquals($move1->y, 19);

		$move1 = $this->botAi->updatePosition(new Point(0, 0), 'D');
		$this->AssertEquals($move1->x, 0);
		$this->AssertEquals($move1->y, 1);

		$move1 = $this->botAi->updatePosition(new Point(2, 3), 'R');
		$this->AssertEquals($move1->x, 3);
		$this->AssertEquals($move1->y, 3);

		$move1 = $this->botAi->updatePosition(new Point(19, 4), 'R');
		$this->AssertEquals($move1->x, 0);
		$this->AssertEquals($move1->y, 4);
	}

	public function testEmptyBoard() {
		$board = $this->botAi->emptyBoard(2, 2);

		$this->AssertEquals(sizeof($board), 2);
	}

	public function testUpdateBoard() {
		$move = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$this->AssertEquals($this->botAi->boardState[1][1], 1);
	}

	public function testIterate() {
		$move = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		for ($i = 0; $i < 1000; $i++) {
			self::$GAME_ROUND->moves[0]->move = $move;
			$move = $this->botAi->makeMove(self::$GAME_ROUND);
			$this->AssertTrue(in_array($move, array('U', 'D', 'L', 'R')));
			$this->dumpBoardState($this->botAi);
		}
	}

	public function testMoveIsGood()
	{
		$move = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$this->botAi->boardState = $this->botAi->emptyBoard(3, 3);
		$pos = new Point(1,1);
		$this->AssertEquals($this->botAi->moveIsGood('U'), true);
		$this->AssertEquals($this->botAi->moveIsGood('D'), true);
		$this->AssertEquals($this->botAi->moveIsGood('L'), true);
		$this->AssertEquals($this->botAi->moveIsGood('R'), true);
		$this->botAi->boardState = array(array(1,1,1), array(1,1,1), array(1,1,1));
		$this->AssertEquals($this->botAi->moveIsGood('U'), false);
		$this->AssertEquals($this->botAi->moveIsGood('D'), false);
		$this->AssertEquals($this->botAi->moveIsGood('L'), false);
		$this->AssertEquals($this->botAi->moveIsGood('R'), false);
		$this->botAi->boardState = array(array(false,false,false), array(false,false,1), array(1,1,1));
		$this->AssertEquals($this->botAi->moveIsGood('U'), true);
		$this->AssertEquals($this->botAi->moveIsGood('D'), false);
		$this->AssertEquals($this->botAi->moveIsGood('L'), true);
		$this->AssertEquals($this->botAi->moveIsGood('R'), false);


	}
	private function makeGameRound($moves)
	{
        $playerMoves = [];
        for ($i = 0; $i < count(self::$PLAYERS); $i++)
        {
            $player = self::$PLAYERS[$i];
            $playerMoves[$i] = new PlayerMove($player->id, $moves[$i]);
        }
        return new GameRound($playerMoves);
	}

	function dumpBoardState($botAi) {
		foreach ($botAi->boardState as  $x => $r1) {
			echo '| ';
			foreach ($r1 as $y => $r2) {
				if ($botAi->positions[1]->x == $x && $botAi->positions[1]->y == $y) {
					echo '@';
				} else {
					echo $r2 === false ? ' ' : $r2;
				}
			}
			echo " |\n";
		}
		echo "\n------------------------------------------\n\n";
		ob_flush();
		usleep(90000);
	}
}
