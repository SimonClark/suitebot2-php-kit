<?php

use PHPUnit\Framework\TestCase;
use Game\Player;
use Game\PlayerMove;
use Game\Point;
use Game\GamePlan;
use Game\GameSetup;
use Game\GameRound;
use Ai\SampleBotAi;

class SampleBotAiTest extends TestCase
{
	const MY_ID = 1;
	const PLAN_WIDTH = 10;
	const PLAN_HEIGHT = 5;
	const MAX_ROUNDS = 4;

    private static $PLAYERS, $STARTING_POSITIONS, $GAME_PLAN, $GAME_SETUP;

	private $botAi;

    public static function setUpBeforeClass()
    {
        self::$PLAYERS = [new Player(1, "Me"), new Player(2, "Enemy 1"), new Player(3, "Enemy 2")];
        self::$STARTING_POSITIONS = [new Point(1, 1), new Point(2, 2), new Point(3, 3)];
        self::$GAME_PLAN = new GamePlan(self::PLAN_WIDTH, self::PLAN_HEIGHT, self::$STARTING_POSITIONS, self::MAX_ROUNDS);
        self::$GAME_SETUP = new GameSetup(self::MY_ID, self::$PLAYERS, self::$GAME_PLAN);

    }

    public function setUp()
    {
        $this->botAi = new SampleBotAi();
    }

	public function testShouldFireIfEnemyFired()
	{
		$this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$myMoveAfterEnemyFired = $this->botAi->makeMove($this->makeGameRound(["MY MOVE", "FIRE", "WARM UP"]));
		$this->assertEquals("FIRE", $myMoveAfterEnemyFired);
	}

	public function testShouldNotFireIfEnemyDidNotFire()
	{
		$myFirstMove = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$this->assertThat($myFirstMove, $this->logicalNot($this->equalTo("FIRE")));
		$myMoveAfterEnemyDidNotFire = $this->botAi->makeMove($this->makeGameRound(["MY MOVE", "WARM UP", "WARM UP"]));
		$this->assertThat($myMoveAfterEnemyDidNotFire, $this->logicalNot($this->equalTo("FIRE")));
	}

	public function testShouldWarmUpInFirstHalf()
	{
		$gameRound = $this->makeGameRound(["MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE"]);
		$myRound1Move = $this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$this->assertEquals("WARM UP", $myRound1Move);
		$myRound2Move = $this->botAi->makeMove($gameRound);
		$this->assertEquals("WARM UP", $myRound2Move);
	}

	public function testShouldNoLongerWarmUpInSecondHalf()
	{
		$gameRound = $this->makeGameRound(["MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE"]);
		$this->botAi->initializeAndMakeMove(self::$GAME_SETUP);
		$this->botAi->makeMove($gameRound);
		$myRound3Move = $this->botAi->makeMove($gameRound);
		$this->assertThat($myRound3Move, $this->logicalNot($this->equalTo("WARM UP")));
		$myRound4Move = $this->botAi->makeMove($gameRound);
		$this->assertThat($myRound4Move, $this->logicalNot($this->equalTo("WARM UP")));
	}

	private function makeGameRound($moves)
	{
        $playerMoves = [];
        for ($i = 0; $i < count(self::$PLAYERS); $i++)
        {
            $player = self::$PLAYERS[$i];
            $playerMoves[$i] = new PlayerMove($player->id, $moves[$i]);
        }
        return new GameRound($playerMoves);
	}
}
